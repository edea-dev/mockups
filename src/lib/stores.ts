import { writable } from 'svelte/store';

export enum ProjectOverview {
	Card,
	ListItem
}

export const projectOverview = writable(ProjectOverview.ListItem);

export enum ProjectPageLayout {
	Grid,
	ReadmeFirst
}

export const projectPageLayout = writable(ProjectPageLayout.Grid);

export enum KicadIntegration {
	PluginContentManager,
	EESchemaOnly
}

export const kicadIntegration = writable(KicadIntegration.PluginContentManager);

export enum CmdNamespace {
	Unqualified,
	Qualified
}

export const cmdNamespace = writable(CmdNamespace.Unqualified);

export const loggedIn = writable(false);
