export const projects = [
	{ title: 'NCP1117 3v3 LDO Regulator', slug: '3v3ldo' },
	{ title: 'GD32E103CBT6 MCU', slug: 'gd32e103cbt6' },
	{ title: 'NCV68261 RP-Protector RP Switch', slug: 'ncv68261' },
	{ title: 'TPS62135 PoL DC-DC Conv.', slug: 'dcdc' },
	{ title: 'HT7533 LDO Precision Regulator', slug: 'ht7533' },
	{ title: 'ADP5061 Multi-Chem Charger', slug: 'adp5061' },
	{ title: 'LP2985 Ultra LDO Regulator', slug: 'lp2985' },
	{ title: 'LT1931 High Eff. DC-DC Conv.', slug: 'lt1931' },
	{ title: 'LM2674 Step-Up DC-DC Conv.', slug: 'lm2674' },
	{ title: 'MC34063 Step-Up/Step-Down DC-DC Conv.', slug: 'mc34063' },
	{ title: 'MC33269 High Eff. DC-DC Conv.', slug: 'mc33269' },
	{ title: 'NCV7608 Low-Side Switch', slug: 'ncv7608' },
	{ title: 'NCP5623 PWM Control', slug: 'ncp5623' },
	{ title: 'NCP1117 LDO Regulator', slug: 'ncp1117' },
	{ title: 'NCP1203 PWM Control', slug: 'ncp1203' },
	{ title: 'NCP1529 LDO Regulator', slug: 'ncp1529' },
	{ title: 'NCP1605 PWM Control', slug: 'ncp1605' },
	{ title: 'NCP1851 High Eff. DC-DC Conv.', slug: 'ncp1851' },
	{ title: 'NCP1852 High Eff. DC-DC Conv.', slug: 'ncp1852' },
	{ title: 'NCP2820 PWM Control', slug: 'ncp2820' }
] as const;

export interface Project {
	title: string;
	slug: string;
}
