import { error } from '@sveltejs/kit';
import { projects } from '$lib/projects';
import type { PageLoad } from './$types';
export const load: PageLoad = async function ({ params }) {
	const project = projects.find(({ slug }) => slug === params.slug);
	if (project != null) {
		return project;
	}
	throw error(404, 'Not found');
};
